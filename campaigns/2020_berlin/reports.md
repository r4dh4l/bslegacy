**Blackstone's Legacy 2020 Berlin campaign report**

# General

## Abbreviations

| Abbreviation | Meaning            |
| ---          | ---                |
| chs          | chosen             |
| CMD(s)       | COMMANDER(s)       |
| MG           | Mission Glory      |
| HVC          | Hidden Vault Hints |
| IgDay(s)     | Ingame Day(s)      |
| rnd          | random             |

## Important links

- Campaign manual/rules: https://gitlab.com/r4dh4l/bslegacy/-/blob/master/manual.md
- Printing templates: https://gitlab.com/r4dh4l/bslegacy/-/tree/master/templates

## Campaign days and schedule

| Date               | Schedule    | Ingame Day Number played |
| ---                | ---         | ---                      |
| Sa, 11th Jan 2020  | 11am to 7pm | 01                       |
| Sa, 25th Jan 2020  | 11am to 7pm | 02                       |
| Sa, 1st Feb 2020   | 11am to 7pm | 03                       |
| Sa, 7th Mar 2020   | 11am to 7pm | ?                        |
| Sa, 4th Apr 2020   | 11am to 7pm | ?                        |
| Sa, 2nd May 2020   | 11am to 7pm | ?                        |
| Sa, 27th June 2020 | 11am to 7pm | ?                        |

- Org. vote: https://poll.digitalcourage.de/ZrVZU1CZmDPgdJ7x

| Time window | Phase         | Duration |
| ---         | ---           | ---      |
| 11:00-11:30 | Preparation   | 30min    |
| 11:30-12:00 | Trading       | 30min    |
| 12:00-15:00 | Mission       | 160min   |
| 15:00-15:30 | (break)       | 30min    |
| 15:30-16:30 | Trading       | 60min    |
| 16:30-18:00 | Arena         | 90min    |
| 18:00-18:30 | (time buffer) | 30min    |
| 18:30-19:00 | Clearance     | 30min    |

## Participants and teams (in alphabetical order)

| Player name | Faction                     | Kill Team name                    | Kill Team ID (KT ID) | Participated on IgDays |
| ---         | ---                         | ---                               | ---                  | ---                    |
| Ali         | Necrons                     | FluffyGismos                      | FG                   | 01,02,03,04            |
| Duc         | Drukhari                    | No Pain No Gain                   | NPNG                 | 01                     |
| Filip       | Death Watch                 | Garbage Collection                | GC                   | 02,03,04               |
| Julian      | Orks                        | Lucky Looters                     | LL                   | 01,02,03               |
| Kilian      | Death Guard                 | Lepra Express                     | LPX                  | 02,03,04               |
| Nuri        | Ork Hunters (Genest. Cults) | Warband of Inquisitor Lahmia Reno | ILR                  | 02                     |
| Philipp     | Adeptus Astartes            | Soriel's Battlegroup              | SB                   | 01,02,03,04            |
| Roland      | T'au Empire                 | For the Greater Legacy            | FtGL                 | 03,04                  |

# Campaign progress

## Overall rankings

### Team ranking

#### Blackstone Fortress missions

| KT ID | Total HVC | Total MG | CMD for next day? | CMDs in battle |
| ---   | ---       | ---      | ---               | ---            |
| FG    | 3         | 8        | no                | 0              |
| LPX   | 2         | 2        | yes: 2 rnd, 2 chs | 2              |
| SB    | 1         | 1        | yes: 2 rnd, 2 chs | 2              |
| GC    | 0         | 8        | no                | 0              |
| FtGL  | 0         | 0        | yes: 2 rnd, 2 chs | 2              |
| IRL   | 0         | 0        | yes: 2 rnd, 2 chs | 2              |
| NPNG  | 0         | -1       | yes: 2 rnd, 2 chs | 2              |
| LL    | 0         | -3       | yes: 2 rnd, 2 chs | 2              |

#### Precipice Arena matches (INCOMPLETE, data missing)

| KT ID | Total Team Glory | Total Team Kills |
| ---   | ---              | ---              |
| FG    | ???              | 3                |
| SB    | ???              | 2                |
| LL    | ???              | 1                |
| LPX   | ???              | 1                |
| FtGL  | -1               | 0                |
| NPNG  | -???             | 0                |

### Unit ranking (Arena plot only)

| Character name    | UID    | Sub-Faction           | Arena Unit Glory | Arena Unit Kills |
| ---               | ---    | ---                   | ---              | ---              |
| Tahar the Impalar | FG02   | Novokh                | 2                | 1                |
| Eknotath          | FG02   | Novokh                | ???              | 2                |
| Ezekial Kaelon    | SB03   | Dark Angels           | ???              | 2                |
| Friedbert         | LPX06  | Dark Raiders          | ???              | 1                |
| Spitburn          | LL03   | Deathskulls           | ???              | 1                |
| Boltz             | LL07   | Deathskulls           | -???             | 0                |
| T'Pol             | FtGL   | Sa'cea                | 0                | -1               |
| Edward            | NPNG01 | Prophets of the Flesh | -???             | 0                |
| Nemator Azdallon  | SB02   | Dark Angels           | -???             | 0                |
| Wildfred          | GC07   | -                     | -???             | 0                |

## Day ranking and mission reports

### Ingame Day 01 (2020-01-11) reports

#### Missions (01)

| KT ID | Total HVC | Total MG | CMD for next day? | Achieved MG | Achieved HVC |
| ---   | ---       | ---                 | ---                     | ---                    | ---          |
| GC    | 0         | 1                   | no                      | +1                     | +1           |
| FG    | 0         | 1                   | no                      | +1                     | +1           |
| SB    | 0         | -1                  | no                      | -1                     | 0            |
| NPNG  | 0         | -1                  | no                      | -1                     | 0            |
| LL    | 0         | -1                  | no                      | -1                     | 0            |
 
**"Soriel's Battlegroup" (SB) VS "Garbage Collection" (GC)**

[![bslegacy2020-01-11-berlin_igday01_12-47-23_cut400.jpg](media/bslegacy2020-01-11-berlin_igday01_12-47-23_cut400.jpg "BSLegacy ingame day 01 - Soriel's Battlegroup VS Filip's Space Marines")](media/bslegacy2020-01-11-berlin_igday01_12-47-23_cut.jpg)

- Mission: AT Hunt
- Result (+/- MG): GC won (+1 GC, -1 GC)
- Anomalies: The Dreaded Ambul appeared.

**"FluffyGismos" (FG) VS "NoPainNoGain" (NPNG) VS "Lucky Looters" (LL)**

[![bslegacy2020-01-11-berlin_igday01_11-46-42_cut400](media/bslegacy2020-01-11-berlin_igday01_11-46-42_cut400.jpg "BSLegacy ingame day 01 - FluffyGismos VS No Pain No Gain VS an Ork Kill Team")](media/bslegacy2020-01-11-berlin_igday01_11-46-42_cut.jpg)

- Mission: HVC Hunt
- Result (+/- MG): FG won (+1 FG, -1 NPNG, -1 LL)

#### Arena (01)

| Character name | UID | Sub-Faction              | Achieved Unit Glory | Achieved Unit Kills |
| ---            | ---    | ---                   | ---                 | ---                 |
| Eknotath       | FG02   | Novokh                | +5                  | +2                  |
| Ezekial Kaelon | SB03   | Dark Angels           | +1                  | +1                  |
| Boltz          | LL07   | Deathskulls           | -1                  | 0                   |
| Edward         | NPNG01 | Prophets of the Flesh | -???                | 0                   |
| Wildfred       | GC07   | -                     | -???                | 0                   |

| KT ID | Total Team Glory | Total Team Kills |
| ---   | ---              | ---              |
| FG    | 5                | 2                |
| SB    | 1                | 1                |
| GC    | -???             | 0                |
| LL    | -???             | 0                |
| NPNG  | -???             | 0                |

**Adeptus Astartes "Ezekial Kaelon" (SB03, 38P, Holo mode) VS against Ork "Boltz" (LL01, ???P, Holo mode)**

Result (+/- AUG): SB03 won (+1 SB03, -1 LL01)

**Necron "Eknotath" (FG02, 25P, Fear no Death mode) VS Drukhari "Edward" (NPNG01, 36P, Fear no Death mode) VS Death Watch "Wildfred" (GC07, 21P, Fear no Death mode)**

Result (+/- AUG): FG02 won (+??? FG02, -??? NPNG01, -??? GC07)

### Ingame Day 02 (2020-01-25) reports

#### Missions (02)

| KT ID | Total HVC | Total MG | CMD for next day? | Achieved MG | Achieved HVC |
| ---   | ---       | ---                 | ---                     | ---                    | ---          |
| FG    | 2         | 2                   | no                      | +1                     | +1           |
| GC    | 0         | 2                   | no                      | +1                     | 0            |
| IRL   | 0         | 1                   | no                      | +1                     | 0            |
| NPNG  | 0         | -1                  | yes (1)                 | 0                      | 0            |
| LPX   | 0         | -1                  | yes (1)                 | -1                     | 0            |
| LL    | 0         | -2                  | yes (2)                 | -1                     | 0            |
| SB    | 0         | -2                  | yes (2)                 | -2                     | 0            |

**"Soriel's Battlegroup" (SB) VS "Warband of Inquisitor Lahmia Reno" (IRL)**

- Mission: AT Hunt
- Result (+/- MG): IRL won (+1 IRL, -1 SB)
- Anomalies: The Dreaded Ambul appeared.

**"FluffyGismos" (FG) VS "Lucky Looters" (LL)**

- Mission: HVC Hunt
- Result (+/- MG): FG won (+1 FG, -1 LL)
- Anomalies: Cultist Firebrand appeared and joined LL, Lost Negavolt Cultist appeared and attcked FG.

**"Garbage Collection" (GC) VS "Lepra Express" (LPX)**

- Mission: AT Hunt
- Result (+/- MG): GC won (+1 GC, -1 LPX)
- Anomalies: A Bloodchaser appeared, but was immediately gunned down by one of the death watch artillery.
- Story/report by LPX: 

> The special Death Watch unit called "Garbage Collection" was destined to meet their end against the "Lepra-Express" (the unstoppable force of death and disease, under direct supervision of Nurgle). But Fate decided to side with the Death Watch.
> 
> Right after the Death Guards front lines of cannon fodder, led by the rotten Constantin the I., entered the dungeon, they were stopped in the midst of their advances. Team Garbage Collection knew, that once their miasma started spreading, there would be no chance of retribution. So they've acted swiftly and precise, like a precisely calculated and quick stab with a shiv. Their specialist for melee combat, armed with a heavy shield, withheld the advancements of the Poxwalkers - one route was successfully blocked. Immediately Nurgle's troops stormed towards the other corridor, only to be awaited by the Death Watch exploration unit - a skilled warrior equipped with one of the fastes jetpacks to exist. No one knew how he had managed to block the second corridor, before even any of team Lepra Express had a chance to notice. Packed like sardines, Nurgles troops had nowhere to go. They couldn't advance and the second wave of troops could not enter the dungeon. This was exactly what team Garbage Collection wanted. The Poxwalkers could not fullfill their mission and storm the area. 
>
> But after a quick moment of irritation, they were ready to club their way through those pesky Marines.  Unfortunately for team Lepra Express, the heavy artillery of team Garbage Collection has already moved into position to unleash a volley of bullets no one could survive. The undead Constantin the I. had immediately noticed what was going on and already readied his plasmagun to take out the Death Guards back line to prevent them from firing. Because even with the Death Guards disgustingly resilient bodily constitution, the inevitable bullet hell, which was about to be unleashed, would rip even the toughest of his troops to shreds. So Constanting acted just as swiftly. Even before the first Marine got a chance to fire, Constantin the I. managed to critically injure the Death Watches Combat specialist, who was wielding one of the three frag cannon, which were destined to annihilate everything. But even in death the Garbage Collection would fullfill their duty.
>
> The falling marine, full of ire, fired into the enemy troups, but didn't manage to kill Constantin. But immediately, after the second two Gunners realized what just happened, they as well started firing. And the sounds of the ripping flesh from the rotten bodies of team Lepra Express was re-echoing all over the dungeon. No one could've survived that. They've managed to take Constantin out of action. But alas, the poxwalkers - only slightly decimated - started their onslaught. They've managed to kill team Garbage Collection's special exploration unit, but had to fall in the process. After that hectic battle team Lepra Express was almost completely anihilated, only three broken Poxwalker were left. They ran for their lives and saved everyone they could. It was a clear victory for team Garbage Collection, although they've tragically lost one of their specialists.

#### Arena (02)

| Character name   | UID   | Sub-Faction  | Achieved Unit Glory | Achieved Unit Kills |
| ---              | ---   | ---          | ---                 | ---                 |
| Eknotath         | FG02  | Novokh       | +1                  | 0                   |
| Spitburn         | LL03  | Deathskulls  | +???                | +???                |
| Nemator Azdallon | SB02  | Dark Angels  | -???                | 0                   |
| Wildfred         | GC07  | -            | -???                | 0                   |
| Friedbert        | LPX06 | Dark Raiders | 0                   | 0                   |

| KT ID | Total Team Glory | Total Team Kills |
| ---   | ---              | ---              |
| FG    | 5                | 2                |
| LL    | -???             | 1/2???           |
| SB    | ???              | 1                |
| GC    | -???             | 0                |
| LPX   | -???             | 0                |

**Necron "Eknotath" (FG02, 25P, Fear no Death mode) VS Death Guard "Friedbert" (LPX06, 19P, Holo mode)**

- Result (+/- AUG): won (+1 FG02, 0 LPX06)
- Anomalie: LPX06 won by always running away.

**Ork "Spitburn" (LL03, ??? (geringste) P, Holo mode) VS Adeptus Astartes "Nemator Azdallon" (SB02, 13P, Holo mode) VS "Wildfred" (GC07, 21P, Fear no Death mode)**

- Result (+/- AUG): LL03 won (+??? LL03, -??? SB02, -??? GC07)

### Ingame Day 03 (2020-02-01) reports

#### Missions (03)

| KT ID | Total HVC | Total MG | CMD for next day? | Achieved MG | Achieved HVC |
| ---   | ---       | ---                 | ---                     | ---                    | ---          |
| FG    | 2         | 0                   | yes (2)                 | -2                     | 0            |
| GC    | 0         | 4                   | no                      | +2                     | 0            |
| LPX   | 2         | 0                   | yes (2)                 | +1                     | +2           |
| SB    | 1         | -1                  | yes (2)                 | +1                     | +1           |
| FtGL  | 0         | 2                   | no                      | 0                      | 0            |
| IRL   | 0         | 1                   | yes (1)                 | +1                     | 0            |
| NPNG  | 0         | -1                  | yes (2)                 | 0                      | 0            |
| LL    | 0         | -3                  | yes (2)                 | -1                     | 0            |

**"Garbage Collection" (GC) VS "Lucky Looters" (LL)**

- Mission: AT Hunt
- Result (+/- MG): GC won (+1 GC, -1 LL)
- Anomalies: -

**"Soriel's Battlegroup" (SB) VS "Lepra Express" (LPX)**

- Mission: HVC Hunt
- Result (+/- MG): LPX won (+1 LPX, -??? SB)
- Anomalies: Shrike appeared, LPX found 1 HVC marauding the battlefield.

**"FluffyGismos" (FG) VS "For the Greater Legacy" (FtGL)**

- Mission: AT Hunt
- Result (+/- MG): FtGL won (+2 FtGL, -2 FG - bonus/malus because of ATG difference before battle)
- Anomalies: Two Ambuls appeared.
- Story/report by FtGL:

> Sha'vre T'pol's Head Up Display was flooded by telemetry. The drones which survived the transport through the debris field around Precipice did a fabulous job in this idiosyncratic environment delivering as much tactical data as possible. Just the interpretation was difficult because the whole complex of the Blackstone Fortress seemed to be a living entity - at least according to the delivered data. While T'Pol tried to fly around to observe the area (which was unexpectedly difficult for a reason she couldn't understand because she was used to handle her XV25 Ghost Suit for years now without any problems) Pathfinder Gunner Scout Lorresa interrupted her fascination for this alien vault: »Shas'vre, enemy units spottet! Signature indicates Necrons! Fire at will?«

[![bslegacy2020-02-01-berlin_igday03_133828_preview.png](media/bslegacy2020-02-01-berlin_igday03_133828_preview.png)](media/bslegacy2020-02-01-berlin_igday03_133828.mp4)

> »Confirmed, Lorresa. Open fire!« - more and more Necrons walked irresistibly down the corridor which connected the vault chambers. T'pol immediately moved all drones in a perimeter formation to protect her team but her combat experience told her: This area was far away from being optimal for general T'au battle tactics. The fire of her burst cannon illuminated the gloomy Fortress vault but she knew that it was somehow gambling attacking Necrons for a certain reason: Plasma bolts which would kill any other unit tend to cure the units known as »living metal«. Firewarrior Breacher Comms Specialist Shas'la Tash'lor opened a channel: »Shas'vre, with all respect - we got all data we need to scout the area. We can come back another day. Shouldn't we withdraw now?« - T'pol knew that her respected comrade was right but her team arrived late at the Blackstone Fortress in comparison to other teams. T'Pols team - For the Greater Legacy - need the ressources in this area to fulfill the mission finding the Hidden Vault Chamber. While her Burst Cannon wrapped Necron warrios one by one she brought herself to order the withdraw: »Attention team, withdraw in %alskd&aslk§« - suddenly her voice was overlain by a huge seismic event. The whole vault was trembling. T'pol couldn't believe what she saw appearing in the middle of the Necron units:

[![bslegacy2020-02-01-berlin_igday03_160417_preview.png](media/bslegacy2020-02-01-berlin_igday03_160417_preview.png)](media/bslegacy2020-02-01-berlin_igday03_160417.mp4)

> While her team collected ArcheoTech out of the dark dust she watched the Necron units laying in the dust tattered by the two monstrous creatures she've never seen before. Completing her mission in such an environment will be the hardest challenge she could imagine. »Ok, load the drones and let's go back to Precipice. The Greater Good was gracious enough today!«

#### Arena (04)

| Character name    | UID    | Sub-Faction  | Achieved Unit Glory | Achieved Unit Kills |
| ---               | ---    | ---          | ---                 | ---                 |
| Tahar the Impalar | FG01   | Novokh       | +2                  | +1                  |
| Boltz             | LL07   | Deathskulls  | -???                | 0                   |
| Ezekial Kaelon    | SB03   | Dark Angels  | +???                | +1                  |
| T'Pol             | FtGL01 | Sa'cea       | -1                  | 0                   |

| KT ID | Total Team Glory | Total Team Kills |
| ---   | ---              | ---              |
| FG    | 7                | 3                |
| LL    | -???             | 1/2???           |
| SB    | ???              | 2                |
| FtGL  | -1               | 0                |

**Necron "Tahar the Impalar" (FG01, 25P, Fear no Death mode) VS against T'au "T'Pol" (FtGL01, 21P, Holo mode)

- Result (+/- AUG): FG01 won (+2 FG01, -1 FtGL01)
- Anomalie: FtGL01 killed by shrinked arena wall

**Adeptus Astartes "Ezekial Kaelon" (SB03, 38P, Holo mode) VS Ork "Sawsnik" (LL16, ??? (weniger) P, ??? mode)

Result (+/- AUG): SB03 won (+??? FG01, -??? LL16)

### Ingame Day 04 (2020-03-07) reports

#### Missions (04)

Attention: How MG is generated was [adjusted](https://gitlab.com/r4dh4l/bslegacy/-/commit/f70a77ed13293229d037b6510b730951b264c17a) by the impression of day 04 so the mechanism COMMANDERs are available or not changed for the next day (05) in comparison to previous days.

| KT ID | Total HVC | Total MG | CMD for next day? | CMDs in battle | Achieved MG             | Achieved HVC |
| ---   | ---       | ---      | ---               | ---            | ---                     | ---          |
| FG    | 3         | 8        | no                | 0              | +4 (+4 for 2 prev. HVC) | 1            |
| LPX   | 2         | 2        | yes: 2 rnd, 2 chs | 2              | -2 (+4 for 2 prev. HVC) | 0            |
| SB    | 1         | 1        | yes: 2 rnd, 2 chs | 2              | +4                      | 1            |
| GC    | 0         | 8        | no                | 0              | +4                      | 0            |
| FtGL  | 0         | 0        | yes: 2 rnd, 2 chs | 2              | -2                      | 0            |
| IRL   | 0         | 0        | yes: 2 rnd, 2 chs | 2              | -1                      | 0            |

**"Garbage Collection" (GC) VS Warband of Inquisitor Lahmia Reno (IRL)**

- Mission: AT Hunt
- Result (+/- MG): GC won (+4 GC, -1 IRL)
- Anomalies: Rat swarm, fruits, Ogre for IRL.

**"Soriel's Battlegroup" (SB) VS "Lepra Express" (LPX)**

- Mission: HVC Hunt
- Result (+/- MG): SB won (+4 SB, -2 LPX)
- Anomalies: Magic cloud appeared, leader of SB was catched by LPX

**"FluffyGismos" (FG) VS "For the Greater Legacy" (FtGL)**

- Mission: HVC Hunt
- Result (+/- MG): FG won (+4 FG, -2 FtGL)
- Anomalies: An Ambul appeared.
- Story/report by FtGL:

"The second day in the Blackstone Fortress - the second time stumbling over a team of Necrons. Shas'vre T'Pol watched the Drones of her team at the end of the alien vault trying to secure the mission target - one of highly wanted hidden vault clues. T'Pol was very proud of these great developments of the Earth Caste - the Drones did not only resist against multiple Necron attacks but against a Dreaded Ambul as well. Although the Necrons luckily entered the Fortress vault finding the mission target right to their feet it was just the Ambul that let them get the mission target. The huge creature prevented all available drones occupying the mission target. Anyway it was a great opportuniy to see the superiority of the Greater Good: The ancient mind control techniques of the Necrons couldn't harm the strong will of the T'au units in any way even without the presence of an Ethereal. Leaving the battlefield T'pol noticed a golden armour in the dust under her feet. A dark voice from the past whispered to her... was it the voice of the Custodian unit? T'pol clenched her fist. The glove of her stealth suit started to deform by the force of her hand. »May we meet again!« she whispered guiding her units back to Precipice."

#### Arena (04)

(no Arena matches played)
