# Blackstones Legacy

A tabletop rule project for a "[Warhammer 40k](https://en.wikipedia.org/wiki/Warhammer_40,000): Kill Team" campaign rule set combining the common rules for "Kill Team" with the setting of "Blackstone Fortress" inspired by [Mordheim](https://en.wikipedia.org/wiki/Mordheim) with a little bit "Shadow War: Armageddon".

- [campaign rule book / manual](https://gitlab.com/r4dh4l/bslegacy/blob/master/manual.md)
- [templates for printing](https://gitlab.com/r4dh4l/bslegacy/tree/master/templates)
- [example of a campaign](https://gitlab.com/r4dh4l/bslegacy/blob/master/campaigns/2020_berlin/reports.md) (startet in late 2019, unfortunately discontinued in early 2020)

[![bslegacy2019-11-16berlin_test01.jpg](media/bslegacy2019-11-16berlin_test01_400.jpg "Bslegacy test game Tau Empire VS Dark Angels")](media/bslegacy2019-11-16berlin_test01.jpg)
